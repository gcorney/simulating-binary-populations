// #! sort axis label positioning


#include <iostream.h>
#include <stdio.h>
#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TLegend.h"



/// Formatting preferences...
	gStyle -> SetOptStat(0);

	//TCanvas* c1 =  new TCanvas();  




int cf_histo(TH1F* histo);


int plotter(){


/// Create default legend
	legend = new TLegend(0.5, 0.5, 0.8, 0.8);
	legend -> SetLineColor(kWhite);
	legend -> SetFillColor(0);
	legend -> SetFillStyle(1001);


/// Creating in and outfiles.
	outfile = new TFile("ComparisonPlots.root", "RECREATE");

	TFile *all_file = new TFile("all_plots.root");
	TFile *det_file = new TFile("detected_plots.root");
	TFile *mil_all_file = new TFile("million_all_plots.root");
	TFile *mil_det_file = new TFile("million_detected_plots.root");


/// Reading in all relavent histos from infiles all (from 400 generated stars), det (detectedable stars from the 400), and mil (1 million stars generated for good comparison stats)
	// mass ratio, q:
	TH1F *q_all = (TH1F*)all_file -> Get("q");
	TH1F *q_det = (TH1F*)det_file -> Get("q");
	TH1F *q_all_mil = (TH1F*)mil_all_file -> Get("q");
	TH1F *q_det_mil = (TH1F*)mil_det_file -> Get("q");	
	// period, P (log10(P)):
  	TH1F *p_all = (TH1F*)all_file -> Get("logP");
	TH1F *p_det = (TH1F*)det_file -> Get("logP");
	TH1F *p_all_mil = (TH1F*)mil_all_file -> Get("logP");
	TH1F *p_det_mil = (TH1F*)mil_det_file -> Get("logP");
	// eccentricity, e:
	TH1F *e_all = (TH1F*)all_file -> Get("e");
	TH1F *e_det = (TH1F*)det_file -> Get("e");
	TH1F *e_all_mil = (TH1F*)mil_all_file -> Get("e");
	TH1F *e_det_mil = (TH1F*)mil_det_file -> Get("e");
	/// inclination, i:
	TH1F *i_all = (TH1F*)all_file -> Get("i");
	TH1F *i_det = (TH1F*)det_file -> Get("i");
	TH1F *i_all_mil = (TH1F*)mil_all_file -> Get("i");
	TH1F *i_det_mil = (TH1F*)mil_det_file -> Get("i");
	/// radial velocity, v:
	TH1F *v_all = (TH1F*)all_file -> Get("V");
	TH1F *v_det = (TH1F*)det_file -> Get("V");
	TH1F *v_all_mil = (TH1F*)mil_all_file -> Get("V");
	TH1F *v_det_mil = (TH1F*)mil_det_file -> Get("V");
	/// difference in radial velocity, dv:
	TH1F *dv_all = (TH1F*)all_file -> Get("V");
	TH1F *dv_det = (TH1F*)det_file -> Get("V");
	TH1F *dv_all_mil = (TH1F*)mil_all_file -> Get("V");
	TH1F *dv_det_mil = (TH1F*)mil_det_file -> Get("V");



//// CUMULATIVE FREQUENCY HISTOGRAMS FOR K-S TEST...


// MASS RATIO:
	/// clone histogram, and pass to function cf_histo() to convert to cumulative frequency histo...
	TH1F *q_det_cf = (TH1F*)q_det->Clone("q_det_cf");
	cf_histo(q_det_cf);	
	q_det_cf -> SetLineColor(kBlue);
	q_det_cf -> GetYaxis() -> SetTitle("Normalized cumulative frequency");
	q_det_cf -> Draw();	

	TH1F *q_all_mil_cf = (TH1F*)q_all_mil->Clone("q_all_mil_cf");
	cf_histo(q_all_mil_cf);	
	q_all_mil_cf -> SetLineColor(kGreen);
	q_all_mil_cf -> Draw("same");

	/// KS test with debug option - outputs P and max distance
 	Double_t ks = q_det_cf->KolmogorovTest(q_all_mil_cf, "D");
	
	/// setting up legend
	legend -> AddEntry(q_det_cf, "Detected binaries", "l");
	legend -> AddEntry(q_all_mil_cf, "Intrinsic distribution", "l");
	legend -> Draw();

	/// saving canvas with plot and clearing used objects
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n\n " << endl;


// PERIOD:
	/// clone histogram, and pass to function cf_histo() to convert to cumulative frequency histo...
	TH1F *p_det_cf = (TH1F*)p_det->Clone("p_det_cf");
	cf_histo(p_det_cf);	
	p_det_cf -> SetLineColor(kBlue);
	p_det_cf -> GetYaxis() -> SetTitle("Normalized cumulative frequency");
	p_det_cf -> Draw();	

	TH1F *p_all_mil_cf = (TH1F*)p_all_mil->Clone("p_all_mil_cf");
	cf_histo(p_all_mil_cf);	
	p_all_mil_cf -> SetLineColor(kGreen);
	p_all_mil_cf -> Draw("same");

	/// KS test with debug option - outputs P and max distance
 	Double_t ks = p_det_cf->KolmogorovTest(p_all_mil_cf, "D");
	
	/// setting up legend
	legend -> AddEntry(p_det_cf, "Detected binaries", "l");
	legend -> AddEntry(p_all_mil_cf, "Intrinsic distribution", "l");
	legend -> Draw();
	
	/// saving canvas with plot and clearing used objects
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n\n " <<endl;


// ECCENTRICITY:
	/// clone histogram, and pass to function cf_histo() to convert to cumulative frequency histo...
	TH1F *e_det_cf = (TH1F*)e_det->Clone("e_det_cf");
	cf_histo(e_det_cf);	
	e_det_cf -> SetLineColor(kBlue);
	e_det_cf -> GetYaxis() -> SetTitle("Normalized cumulative frequency");
	e_det_cf -> Draw();	

	TH1F *e_all_mil_cf = (TH1F*)e_all_mil->Clone("e_all_mil_cf");
	cf_histo(e_all_mil_cf);	
	e_all_mil_cf -> SetLineColor(kGreen);
	e_all_mil_cf -> Draw("same");

	/// KS test with debug option - outputs P and max distance
 	Double_t ks = e_det_cf->KolmogorovTest(e_all_mil_cf, "D");
	
	/// setting up legend
	legend -> AddEntry(e_det_cf, "Detected binaries", "l");
	legend -> AddEntry(e_all_mil_cf, "Intrinsic distribution", "l");
	legend -> Draw();
	
	/// saving canvas with plot and clearing used objects
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n\n " <<endl;



// INCLINATION:
	/// clone histogram, and pass to function cf_histo() to convert to cumulative frequency histo...
	TH1F *i_det_cf = (TH1F*)i_det->Clone("i_det_cf");
	cf_histo(i_det_cf);	
	i_det_cf -> SetLineColor(kBlue);
	i_det_cf -> GetYaxis() -> SetTitle("Normalized cumulative frequency");
	i_det_cf -> Draw();	

	TH1F *i_all_mil_cf = (TH1F*)i_all_mil->Clone("i_all_mil_cf");
	cf_histo(i_all_mil_cf);	
	i_all_mil_cf -> SetLineColor(kGreen);
	i_all_mil_cf -> Draw("same");

	/// KS test with debug option - outputs P and max distance
 	Double_t ks = i_det_cf->KolmogorovTest(i_all_mil_cf, "D");
	
	/// setting up legend
	legend -> AddEntry(i_det_cf, "Detected binaries", "l");
	legend -> AddEntry(i_all_mil_cf, "Intrinsic distribution", "l");
	legend -> Draw();
	
	/// saving canvas with plot and clearing used objects
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n\n " <<endl;



	// TESTING KS... e_det against q_mil distribution KS & plotted - should return false
 	Double_t ks = e_det_cf->KolmogorovTest(q_all_mil_cf, "D");

	e_det_cf -> Draw();	
	q_all_mil_cf -> Draw("same");
	legend -> AddEntry(e_det_cf, "Detected binary cumulative values of e", "l");
	legend -> AddEntry(q_all_mil_cf, "Intrinsic cumulative distribution of period", "l");
	legend -> Draw();
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n\n " <<endl;






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// DISPLAYING BIAS OF ALL VS DETECTED, USING MILLION SIMULATION DATA ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///// mass ratio, q:
	// format, colour and draw both histograms on same canvas:
	//q_all_mil -> Rebin(5);
	q_all_mil -> SetLineColor(kRed);
	q_all_mil -> Draw();	
	//q_det_mil -> Rebin(5);
	q_det_mil -> SetLineColor(kBlue);
	q_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(q_all_mil, "Intrinsic binaries", "l");
	legend -> AddEntry(q_det_mil, "Detected binaries", "l");
	legend -> Draw();

	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;



///// period, P:
	// format, colour and draw both histograms on same canvas:
	//p_all_mil -> Rebin(2);
	p_all_mil -> SetLineColor(kRed);
	p_all_mil -> Draw();
	//p_det_mil -> Rebin(2);
	p_det_mil -> SetLineColor(kBlue);
	p_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(p_all_mil, "Intrinsic Binaries", "l");
	legend -> AddEntry(p_det_mil, "Detected binaries", "l");
	legend -> Draw();
	
	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;




 ///// eccentricity, e:
	// format, colour and draw both histograms on same canvas:
	//e_all_mil -> Rebin(2);
	e_all_mil -> SetLineColor(kRed);
	e_all_mil -> GetXaxis() -> SetTitle("Eccentricity");
	e_all_mil -> Draw();
	//e_det_mil -> Rebin(2);
	e_det_mil -> SetLineColor(kBlue);
	e_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(e_all_mil, "Intrinsic Binaries", "l");
	legend -> AddEntry(e_det_mil, "Detected binaries", "l");
	legend -> Draw();

	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;



 ///// inclination, i:
	// format, colour and draw both histograms on same canvas:
	//i_all_mil -> Rebin(2);
	i_all_mil -> SetLineColor(kRed);
	i_all_mil -> Draw();
	//i_det_mil -> Rebin(2);
	i_det_mil -> SetLineColor(kBlue);
	i_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(i_all_mil, "Intrinsic Binaries", "l");
	legend -> AddEntry(i_det_mil, "Detected binaries", "l");
	legend -> Draw();

	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;


 ///// radial velocity, v:
	// format, colour and draw both histograms on same canvas:
	//v_all_mil -> Rebin(2);
	v_all_mil -> SetLineColor(kRed);
	v_all_mil -> Draw();
	//v_det_mil -> Rebin(2);
	v_det_mil -> SetLineColor(kBlue);
	v_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(v_all_mil, "Intrinsic Binaries", "l");
	legend -> AddEntry(v_det_mil, "Detected binaries", "l");
	legend -> Draw();

	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;


 ///// difference in radial velocity, dv:
	// format, colour and draw both histograms on same canvas:
	//dv_all_mil -> Rebin(2);
	dv_all_mil -> SetLineColor(kRed);
	dv_all_mil -> Draw();
	//dv_det_mil -> Rebin(2);
	dv_det_mil -> SetLineColor(kBlue);
	dv_det_mil -> Draw("same");

	// add legend entries and draw legend:
	legend -> AddEntry(dv_all_mil, "Intrinsic Binaries", "l");
	legend -> AddEntry(dv_det_mil, "Detected binaries", "l");
	legend -> Draw();

	// save canvas to outfile and clear used canvas and legend:
	outfile -> WriteTObject(c1);
	c1 -> Clear();
	c1 -> Close();
	legend -> Clear();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << endl;










  return 0;
}




int cf_histo(TH1F* histo){

	histo -> Scale(1); // to normalise
	histo -> ComputeIntegral();
	Double_t* integral = histo->GetIntegral();
	histo -> SetContent(integral);	

return 0;
}

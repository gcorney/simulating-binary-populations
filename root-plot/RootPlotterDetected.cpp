/*

This code reads in the output results of the simulation of binary stars for detected systems only. It then uses ROOT to plot the distributions of various binary parameters.

*/



#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#define FALSE 0
#define TRUE 1
 


using namespace std;
TFile* outfile = new TFile("detected_plots.root", "RECREATE");


///// Define output histograms TH1F(name,title,nBins,xMin,xMax) ///// 
TH1F* eccentricity_hist = new TH1F("e","Recovered eccentricity distribution from detectable binaries", 200, 0, 1);
TH1F* mass_ratio_hist = new TH1F("q", "Recovered mass ratio distribution from detectable binaries", 200, 0, 1);
TH1F* period_hist = new TH1F("P", "Recovered period distribution from detectable binaries", 1000, 0, 1000);
TH1F* log_period_hist = new TH1F("logP", "Recovered log period distribution", 350, 0, 3.5);
TH1F* longPeri_hist = new TH1F("omega", "Recovered longitude of periastron distribution from detectable binaries", 350, 0, 3.5); 
TH1F* inclination_hist = new TH1F("i", "Recovered inclination distribution from detectable binaries", 200, 0, 2);
TH1F* aStar_hist = new TH1F("a*", "Recovered distribution of semi major axis about centre of mass from detectable binaries", 200, 0, 2E12);
TH1F* a_hist = new TH1F("a", "Recovered distribution of the binaries semi major axis from detectable binaries", 500, 0, 5E12);
TH1F* trueAnom_hist = new TH1F("trueAnom", "Recovered true anomaly distribution from detectable binaries", 700, 0, 7);
TH1F* eccAnom_hist = new TH1F("eccAnom", "Recovered ecc anomaly distribution from detectable binaries", 700, 0, 7);
TH1F* meanAnom_hist = new TH1F("meanAnom", "Recovered mean anomaly distribution from detectable binaries", 700, 0, 7);
TH1F* velocity_hist = new TH1F("V", "Observed radial velocity distribution from detectable binaries", 1000, -500, 500);
TH1F* dV_hist = new TH1F("dV", "Observed distribution of radial velocity difference from detectable binaries", 2000, 0, 800);

int RootPlotterDetected(){	
   float eccentricity=0, massRatio=0, period=0, longPeri=0, inclination=0, aStar=0, a=0, trueAnomaly=0, eccAnomaly=0, meanAnomaly=0, obsRadVel=0, dV=0;
	float logPeriod=0;
	string line = " ";
	int line_count = 0;
	int binary_count = 0;

	FILE* inputfile = fopen("detected.txt", "read");
	ifstream infile("detected.txt");
	if(infile.is_open()){
		while(!infile.eof()){	
      		getline(infile, line);  
     	 	if(line[0] == '\0') continue;

 			//scanf split up due to root limit on number of args
			fscanf(inputfile,"%g %g %g %g %g %g", &eccentricity, &massRatio, &period, &longPeri, &inclination, &aStar);
			fscanf(inputfile,"%g %g %g %g %g %g", &a, &trueAnomaly, &eccAnomaly, &meanAnomaly, &obsRadVel, &dV);
			
			logPeriod = log10(period);
			obsRadVel = obsRadVel/1000; // m/s to km/s
			dV = dV/1000; // m/s to km/s

			///// fills the parameter (read in from the current line of infile) into the relavent histogram
			eccentricity_hist -> Fill(eccentricity);
			mass_ratio_hist -> Fill(massRatio);
			period_hist -> Fill(period);
			log_period_hist -> Fill(logPeriod);
			longPeri_hist -> Fill(longPeri);
			inclination_hist -> Fill(inclination);
			aStar_hist -> Fill(aStar);
			a_hist -> Fill(a);
			trueAnom_hist -> Fill(trueAnomaly);
			eccAnom_hist -> Fill(eccAnomaly);
			meanAnom_hist -> Fill(meanAnomaly);
			velocity_hist -> Fill(obsRadVel);
			dV_hist -> Fill(dV);

			line_count++;
		}
	}
	else cout << "Error, could not open file" << endl;


		cout << "lines in file: " << line_count << endl; 
		cout << "binary count: " << binary_count << endl;

///// Setting axis titles and drawing histocgrams...
   eccentricity_hist -> GetXaxis()->SetTitle("Eccentricity [radians]");
	eccentricity_hist -> GetYaxis()->SetTitle("Number of binary stars");
	eccentricity_hist -> Draw();

   mass_ratio_hist -> GetXaxis()->SetTitle("Mass ratio (m/M(O-Star))");
	mass_ratio_hist -> GetYaxis()->SetTitle("Number of binary stars");
	mass_ratio_hist -> Draw();

   period_hist -> GetXaxis()->SetTitle("Period [days]");
	period_hist -> GetYaxis()->SetTitle("Number of binary stars");
	period_hist -> Draw();

   longPeri_hist -> GetXaxis()->SetTitle("Longitude of periastron [radians]");
	longPeri_hist -> GetYaxis()->SetTitle("Number of binary stars");
	longPeri_hist -> Draw();

	inclination_hist -> GetXaxis()->SetTitle("Inclination of binary system [radians]");
	inclination_hist -> GetYaxis()->SetTitle("Number of binary stars");
	inclination_hist -> Draw();
	
	aStar_hist -> GetXaxis()->SetTitle("Semi major axis between O-Star and the binaries centre of mass [m]");
	aStar_hist -> GetYaxis()->SetTitle("Number of binary stars");
	aStar_hist -> Draw();

	a_hist -> GetXaxis()->SetTitle("Semi major axis of binary system [m]");
	a_hist -> GetYaxis()->SetTitle("Number of binary stars");
	a_hist -> Draw();

	trueAnom_hist -> GetXaxis()->SetTitle("True Anomaly [radians]");
	trueAnom_hist -> GetYaxis()->SetTitle("Number of binary stars");
	trueAnom_hist -> Draw();

	eccAnom_hist -> GetXaxis()->SetTitle("Eccentric Anomaly [radians]");
	eccAnom_hist -> GetYaxis()->SetTitle("Number of binary stars");
	eccAnom_hist -> Draw();

	meanAnom_hist -> GetXaxis()->SetTitle("Mean Anomaly");
	meanAnom_hist -> GetYaxis()->SetTitle("Number of binary stars");
	meanAnom_hist -> Draw();

	velocity_hist -> GetXaxis()->SetTitle("Observed radial velocity [km/s]");
	velocity_hist -> GetYaxis()->SetTitle("Number of binary stars");
	velocity_hist -> Draw();

	dV_hist -> GetXaxis()->SetTitle("Observed radial velocity difference [km/s]");
	dV_hist -> GetYaxis()->SetTitle("Number of binary stars");
	dV_hist -> Draw();

/////write histos to outfile, close files and canvas c1...
	outfile->Write();
	outfile->Close();
	c1 -> Close();	
	infile.close();
	fclose(inputfile);

return 0;
}

#ifndef RANDOMBINARYGENERATOR_H
#define RANDOMBINARYGENERATOR_H
#include <stdio.h>

class Binary 
{
private:
    double m_massRatio;           // $m / M_{OStar}$
    double m_period;              // in Days
    double m_eccentricity;        // unitless

    double m_meanAnomaly;         // radians, AnomalyConversions converts between anomalies
    double m_eccentricAnomaly;    // radians
    double m_trueAnomaly;         // radians

    double m_longitudePeriastron; // omega
    double m_inclination;         // Radians

    double m_a;                   // meters
    double m_aStar;               // meters

	 double m_DV; 						// m/s 

    double calculate_a();
    double calculate_aStar();

public:
    Binary();
    Binary(
        double massRatio, 
        double period, 
        double eccentricity, 
        double meanAnomaly,
        double eccentricAnomaly, 
        double trueAnomaly, 
        double longitudePeriastron, 
        double inclination, 
        double aStar, 
        double a);
    ~Binary(); // destructor

    inline double getMassRatio()        const { return m_massRatio; }
    inline double getMeanAnomaly()      const { return m_meanAnomaly; }
    inline double getTrueAnomaly()      const { return m_trueAnomaly; }
    inline double getEccentricAnomaly() const { return m_eccentricAnomaly; }
	 
	 void setVelocityDifference(double dV); 
    void progressOrbit(double days);
    double observedRadialVelocity() const; // m/s

    static void printTSVHeader(FILE* filename = stdout);
    void printTSV(FILE* filename = stdout) const;

    //static
    static const double O_STAR_AVERAGE_MASS;
};

#endif

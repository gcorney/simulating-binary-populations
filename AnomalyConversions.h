#ifndef ANOMALYCONVERISONS_H
#define ANOMALYCONVERISONS_H

double meanToTrue(double M, double e);
double meanToEccentric(double M, double e);
double eccentricToTrue(double E, double e);

#endif
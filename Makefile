BINDIR := bin
BUILDDIR := build
SOURCEDIR := ./

OS := $(shell uname)
CXXFLAGS += -Wall -g -std=c++11

SOURCES := $(wildcard $(SOURCEDIR)/*.cpp)
OBJECTS := $(SOURCES:%.cpp=$(BUILDDIR)/%.o)
EXECUTABLE = $(BINDIR)/OStarBinaries

ifeq ($(OS), Darwin)
	CXX ?= clang++
	CXXFLAGS += -stdlib=libc++
else
	CXX ?= g++
endif

all: $(BINDIR) $(BUILDDIR) $(EXECUTABLE)

$(BUILDDIR)/%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $< -MD -MF $(patsubst %.o,%.d,$@)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJECTS)

$(BINDIR):
	mkdir -p $@

$(BUILDDIR):
	mkdir -p $@

clean:
	rm -f *.o *.d
	rm -f -r $(BUILDDIR)
	rm -f -r $(BINDIR)

-include $(OBJECTS:%.o=%.d)
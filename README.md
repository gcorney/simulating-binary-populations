#Gameplan:
##Q6
- #### For starters: generate graphs demonstraiting the introduced detection biases described, by plotting observed data against intrisic distributions.

- #### Use fitting techniques to find the distributions

- ####Ideally: graphs quantifying the bias by means of residuals or other methods. & Mention / investigate KS test for each distribution

##Q7
- ####Fiddle with numbers and repeat some steps in Q6

---

#Q6

Using appropriate statistical tests, compare your ‘observed’ population with the input/intrinsic distributions.

Questions you may want to 
consider are:

### Does the observed binary fraction agree with the input fraction? 
	It's less than the input fraction. Resons clear

### Do the recovered mass ratio, period, etc distributions agree with the input ones?
 	No, there are biases:
	- Period: Reduced at large periods
	- Mass Ratio: Skewed towards greater mass distributions
	- Eccentricities: Weak lack of lowest eccentricities, hard to spot
	- Inclination: Skewed towards larged inclinations - lack of lower inclinations and an overabundance of larger ones

### What are the biasses in your observed sample of stars?
	As above + graphs

#Q7
Investigating the power of your observing program to study the binary population. 

Generate two ‘observed’ with different binary fractions.

Can you tell them apart?

You might want to investigate how this is affected by your input distributions, or whether your study can differentiate between two different mass ratio distributions, for example.


---
# NOTES

'#!' indicates a todo in the code

# DONES:

> - Inclination needs confirming (is the range and distribution correct?)
> - Orbital velocity calculations need confirming
> - Detection threshold needs to be set to the resolution limit of the VLT

> - how does mass ratio affect period? are independent distributions okay? - ask in workshop 2
> - aStar: semi major axis of star about CofM, needed for the radial velocity
> -     K = (2pi * aStar * sini)/(P * sqrt(1-e^2))
> -     V = K(cos(trueAnom+omega)+ecos(omega))
> - a: semi major axis of binary (enters Kepler's 3rd Law). - ask in workshop 2
> -     Kepler's 3rd law: P^2 = 4pi/G(M1+M2) * a^3
> - V: radial velocity function needed (equation in q1 of script)
> - rethink functions or methods
> - vector of classes (star) so can create 200 rand stars
> - think about limitations on measued radial velocity and how the star params can be derrived from this
 
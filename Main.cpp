#include <iostream>
#include <vector>
#include <cmath>

#include "RandomBinaryGenerator.h"
#include "Random.h"
#include "Parameters.h"

using std::cout;
using std::endl;
using std::string;

//Prototypes
bool isDetected(double v_i, double v_j);
char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);

int main(int argc, const char * argv[])
{   
	int totalObjectsObserved = 400;
	string outDir = "./root-plot";
	bool printBinaries, printDeltas, printHeadersToFile;

	//read command line args	
		//num objects
	if(argc>1){
		totalObjectsObserved = atoi(argv[1]);
	}
	//if(cmdOptionExists((char **)argv, (char **)argv+argc, "-n")) totalObjectsObserved = atoi(getCmdOption((char **)argv, (char **)argv+argc,"-n"));
		//print-binaries flag, -p
	printBinaries      = cmdOptionExists((char **)argv, (char **)argv+argc, "-p");
		//print-rv deltas flag, -p
	printDeltas        = cmdOptionExists((char **)argv, (char **)argv+argc, "-d");
	printHeadersToFile = cmdOptionExists((char **)argv, (char **)argv+argc, "-h");
	if(cmdOptionExists((char **)argv, (char **)argv+argc, "-o")) outDir = getCmdOption((char **)argv, (char **)argv+argc,"-o");

	FILE* allFile      = fopen((outDir+"/all.txt").c_str(),"write");
	FILE* detectedFile = fopen((outDir+"/detected.txt").c_str(),"write");
	FILE* logFile      = fopen((outDir+"/log.txt").c_str(),"write");

	if(!allFile || !detectedFile || !logFile) {
		fprintf(stderr, "Error opening output file\n");
		abort();
	}

	if(printBinaries) Binary::printTSVHeader();
	if(printHeadersToFile) {
		Binary::printTSVHeader(allFile);
		Binary::printTSVHeader(detectedFile);
	}
	//Generate and loop through binaries
	int intrinsicBinaryCount = 0;
	int detectedBinaryCount = 0;
	double u, v_i, v_j, DV;
	for(int i = 0; i<totalObjectsObserved; i++){
		//Binary fraction test
		u = uniformRand(0,1);
		if(u > BINARY_FRACTION){
			//not a binary
			continue;
		}

		intrinsicBinaryCount ++;

		//Generate binary
		Binary binary;
		v_i = binary.observedRadialVelocity();
		//progress through 3-6 months
		binary.progressOrbit(uniformRand(3*30, 6*30));
		v_j = binary.observedRadialVelocity();

		DV = abs(v_j-v_i);
		//cout << " V1: " <<v_j << "\t V2: " << v_i << "\t dV: "<< DV << endl; 
		binary.setVelocityDifference(DV);

		if(isDetected(v_i, v_j)){
			//Detected as binary	
			detectedBinaryCount++;
			binary.printTSV(detectedFile);
		}

		if(printBinaries) binary.printTSV();
		if(printDeltas) cout << "Delta RV " << abs(v_j - v_i)/1000 << endl;
		//output binary properties to file
		binary.printTSV(allFile);
	}
	//Print log
	const char * resultsLog = "\n%-5d OStars observed\n%-5d of which were intrinsically binaries\n%-5d were detected as binaries\n%-5.3f is the observed binary fraction\n\n";

	printf(resultsLog,
		totalObjectsObserved, intrinsicBinaryCount, detectedBinaryCount, (float)detectedBinaryCount/totalObjectsObserved);

	//Write logfile
	fprintf(logFile, resultsLog,
		totalObjectsObserved, intrinsicBinaryCount, detectedBinaryCount, (float)detectedBinaryCount/totalObjectsObserved);
	fprintf(logFile, "#Parameters\nMASSRATIO_KAPPA %.3f\nPERIOD_PI %.3f\nECCENTRICITY_ETA %.3f\nBINARY_FRACTION %.3f\nRV_DETECTION_THRESHOLD %.3f\n",
		MASSRATIO_KAPPA, PERIOD_PI, ECCENTRICITY_ETA, BINARY_FRACTION, RV_DETECTION_THRESHOLD);

	fclose(allFile);
	fclose(detectedFile);
	fclose(logFile);
}

//m / s
bool isDetected(double v_i, double v_j){
	double DV = abs(v_j - v_i);
	double o_i, o_j; // $\sigma_i, \sigma_j$
	o_i = o_j = 2.54*1000;//calculated from averages provided in the tarantula paper
	return DV/sqrt(abs(o_i*o_i + o_j*o_j)) > 4 && DV > 20*1000;
}

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% 元気ですか？ ：ー）
% あまり良い、たくさんよく。あなたがどれだけ、あなたに感謝

\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}

%%% Paper size
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper}
\usepackage{fullpage}

%%% Useful Packages
\usepackage{graphicx}
\usepackage[parfill]{parskip}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float   
\usepackage{amsmath}
\usepackage{wasysym} % \astrosun
\usepackage[backend=bibtex, style=authoryear]{biblatex}
\usepackage{multicol}
\usepackage{fixltx2e}
\usepackage{listings}
\usepackage{pifont} % dingbats

%%% Headers & Footers
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

% multcol
\setlength{\columnsep}{1cm}

%%% Section Styles
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\linecontinue}{\ding{219}}
\newcommand{\unit}[1]{\ensuremath{\, \mathrm{#1}}}

%%% In-column figure
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

%%% Bibliography
\bibliography{phy350_bib}
\def\aap{A\&A}
\def\mnras{Monthly Notices of the Royal Astronomical Society}
%%%%%%%%%%%%%%%%%%%%
%%% Document

\title{Simulating Binary Populations}
\author{
	Jack Batty \and
	George Corney \and
	Sam Hilton \and
	Chris Murphy \and
	Rachel Sellars \and
	Owen Shepherd
}
%\date{}

\begin{document}

\maketitle
\begin{abstract}
There are inherent biases involved in the spectroscopic observation of binaries. As preparation
for an observing campaign with the VLT, we simulated a population of binary 
stars in order to investigate and analyse these biases. A binary model was defined and used to generate a population of 400 O-stars, the intrinsic parameters of which follow assumed distributions taken from \cite{tarantulaviii}. From this, the fraction that would be identified as binaries, given radial velocity measurements taken 3 to 6 months apart, is determined to be $0.37\pm0.02$, substantially lower than the input fraction of $0.51$. The resulting observations were skewed towards binaries with shorter periods, smaller mass ratios and greater average inclination with eccentricity showing only minor biases.
\end{abstract}

\begin{multicols}{2}
\section{Introduction}
As part of a collaboration, The VLT FLAMES spectrograph is to be used to investigated a 400 strong sample of O-Stars. Each star is observed twice over a 3 - 6 month period and the difference in radial velocity is used to classify the systems binarity with the aim ultimately determine the intrinsic binary fraction of the target region. The nature of spectrographic observation leaves the experiment subject to significant observational biases; the objective of this work is to quantify these biases, enabling the intrinsic distribution of systems and their parameters to be estimated.

\vfill % hacky fix
\columnbreak

\section{Background}
\subsection{Anomalies}
\label{bkgd_anomalies}
The true anomaly, $\nu$, is an angle made between the periastron (closest approach
to the centre of mass of the system), and the current location of the body in 
its orbit. Whilst it does define the position of the orbiting body, it does not 
hold any information about where that body will be after a certain period of 
time due to the fact that bodies in elliptical orbits do not progress at a constant 
speed. Since the true anomaly is a function of time and position, it is useful 
to define two different types of anomaly that can be used to represent both 
quantities separately.

The mean anomaly is a point, $M$, on the auxiliary circle (shown in green on 
Figure~\ref{fig_anomaly}). It is an imaginary point that orbits on the circular path 
with a constant speed that has the same orbital 
period as the body with the elliptical orbit, making it easy to 
predict where the mean anomaly will be after a given period of time.

\begin{Figure}
	\centering
	\includegraphics[width=\textwidth]{AnomalyDiagram1.pdf}
	\captionof{figure}{Definition of true anomaly $v$ and eccentric anomaly $E$ for 
	orbiting body $P$}
	\label{fig_anomaly}
\end{Figure}

The eccentric anomaly, $E$, is an angle measured in the auxiliary circle that is
defined by the position of point $P$, as is shown in Figure~\ref{fig_anomaly}. It is an
indication of the position of the orbiting body, and is related to the mean 
anomaly, where e is the eccentricity of the elliptical orbit.
\begin{equation}
	M =  E - e \cdot \sin E
	\label{mean_from_eccentric}
\end{equation}
Both the time, in terms of the mean anomaly, and the position, in terms of the 
eccentric anomaly can be used to calculate the true anomaly. After calculating 
the eccentric anomaly from \eqref{mean_from_eccentric}, the true anomaly can be 
calculated using \eqref{true_from_eccentric} \parencite{orbital_mechanics}.
\begin{equation}
	\tan{\frac{E}{2}} =\sqrt{\frac{1-e}{1+e}} \cdot \tan{\frac{\nu }{2}}
	\label{true_from_eccentric}
\end{equation}

\subsection{Spectral Resolution}
The data is to be obtained using the FLAMES spectrograph, coupled with the 
GIRAFFE spectrograph in low resolution mode, on the VLT. The GIRAFFE is to be 
used  with the Medusa fibres, allowing up to 132 objects to be observed in a 
single exposure. In this setup, the maximum possible resolving power is 
8600 \parencite{eso_flames}. The inverse of the spectral resolving power is equal to 
the equation for non-relativistic Doppler shift \parencite{phy217}:

	\begin{equation}
		\frac{1}{R}=\frac{\Delta \lambda }{\lambda }=\frac{v}{c}
	\end{equation}

Where
	$R$ is resolving power,
	$\lambda$ is the observed wavelength, 
	$v$ is radial velocity, and
	$c$ is speed of light in vacuum.
Entering the values, this gives a radial velocity of $34.9 \unit{kms^{-1}}$. This is the 
minimum radial velocity that can be measured by the telescope in the selected 
mode.

Per \textcite{mahy2013}, an O-Star can be considered a part of a binary system if the radial velocity of
the star has a standard deviation $\ge 7\unit{kms^{-1}}$ and $\ge 20\unit{kms^{-1}}$ for stars of high
rotational speed.

\subsection{Typical Mass of O-Stars}
A key parameter in these simulations is the mass of a typical O-Star. The 
Galactic O-Star Catalog \parencite{gosc} overwhelmingly shows that O-Stars tend 
towards lower luminosities, as would be expected from the general trend through
the stellar classifications. Over half of all classified stars fall from O8 to
O9.7, the majority of them O(V); the median is therefore taken to be of O8(V) 
spectral classification.

\textcite{martins2005} compiled a catalog of parameters for O type stars, 
including masses, for luminosity classes I, III and V and spectral types 9.5 to 
3, giving a value $21.25M_{\astrosun}$ for stars of class O8(V).

\section{Method}
A Monte-Carlo Simulation of the stellar population was used to generate a collection of simulated systems with their associated properties. The resulting population was then analysed to determine what we would observe using an ideal telescope, then based upon this and decide whether such an observation would be classified as a binary using the ESO VLT. 

\subsection{Distribution of Binary Properties of O-Stars}
\label{distributionsource}
Using the largest and most recent sample, composed of 360 objects within ideal 
viewing distance in the LMC, \textcite{tarantulaviii} appears to provide the 
most reliable estimates for the distributions of O-Star binary properties. The 
estimated distributions are consistent with expected constraints (such as 
uniformity across the field of view), and agree well with the results of similar 
research such as \textcite{sana2009}, which, whilst using a similar method, estimates 
have been extended with the inclusion of additional uncertainties and a larger 
sample \parencite[pg. 8]{tarantulaviii}. In the case of \textcite{sana2012}, the 
binary fraction is found to be ~30\% larger, but this has been explained a 
result of a sampling of lower mass stars.

From \textcite{tarantulaviii}, the intrinsic binary fraction is found to be 0.51. The mass ratios, orbital periods and 
eccentricities of the binary population are described by power laws. The probability density functions relating to the parameters are as follows:

orbital period, 
	\begin{equation}
		f(P) = \left( \log_{10}\left( p_{\textrm{days}} \right) \right)^\pi
	\end{equation}
mass ratio 
	\begin{equation}
		f(q) = q^\kappa
	\end{equation}
and eccentricity.
	\begin{equation}
		f(e)=e^\eta
	\end{equation}
with constants $\pi=-0.45\pm0.3$, $\kappa=-1.0\pm0.4$ and $\eta=-0.5$.

Longitude of periastron, $\omega$, is distributed uniformly whereas inclination is
sinusoidally distributed, this result follows from the form 
of a uniform spherical distribution; points picked uniformly on a sphere are given
in spherical coordinates by:
\begin{equation}
	\theta = 2\pi u \\
\end{equation}
\begin{equation}
	\phi = \cos^{-1}(2v-1)
\end{equation}
where $u$ and $v$ are uniforms from $0$ to $1$. It can be shown that the distribution of angles to a
given direction (representing the normal to the reference plane) transformed into inclination convention is given by
\begin{equation}
	i = \cos^{-1}(u)
\end{equation}

The 64-bit variation of the Mesernene Twister random number generator 
\parencite{MeserneneTwister} was used in order to ensure a highly uniform distribution
of numbers were produced.

\subsection{Radial Velocity Determination}
To derive the radial velocity $v_r$, defining the plane of the sky as the $x-y$ plane, the line-of-sight to the observer 
will be along the $z$-axis. This can be expressed as 

\begin{equation}
	z = r \sin(\nu+\omega) \sin{i}
\end{equation}
where 
	$i$ is inclination of orbit,
	$\nu$ is true anomaly,
	$\omega$ is longitude of periastron and
	$r$ is distance of star from centre of mass.

The radial velocity $v_r$ is the change in velocity along the line-of-sight of 
the observer, defined in this case as $z$. From this an expression for $v_r$ can
be obtained by finding the derivative $\frac{dz}{dt}$,

\begin{equation}
	v_{r} \equiv \frac{dz}{dt} = \sin{i} (r \dot{\nu} \cos(\nu + \omega) + \dot{r} \sin(\nu +\omega))
	\label{rvderiv_Vr}
	% (1)
\end{equation}

Using the polar equation for an ellipse
\begin{equation}
	r = a\frac{(1-e^{2})}{(1+e\cos{\nu})}
	\label{rvderiv_r}
	% (2)
\end{equation}
where $a = \textrm{semi major axis of star}$, we find
\begin{equation}
	\dot{r} = \frac{r \dot{\nu} e\sin\nu}{ 1 + e\cos\nu}
	\label{rvderiv_rdot}
	% (3)
\end{equation}
and Kepler's second law
\begin{equation}
	r^{2} \dot{\nu} = 2\pi a^{2} (1-e^{2})^{\frac{1}{2}}
	\label{rvderiv_r2theta}
	% (4)
\end{equation}

From substituting \eqref{rvderiv_r}, \eqref{rvderiv_rdot} and 
\eqref{rvderiv_r2theta} into equation \eqref{rvderiv_Vr}, 
\begin{multline}
	v_{r} = K(e\sin\nu\sin(\nu + \omega) + \\ \cos(\nu +\omega)(1+e\cos\nu))
	\label{rvderiv_vr2}
	% (5)
\end{multline}
which can be simplified by the use of trigonometric identities to
\parencite{ClubbRVDerivation}\parencite{HilditchCloseBinaries}
\begin{equation}
	v_{r} = K(e\cos{\omega} + \cos{\nu +\omega})
	\label{radial_velocity}
\end{equation}
where
\begin{equation*}
	K = \frac{2\pi a\sin i}{P(1-e^{2})^{\frac{1}{2}}}
\end{equation*}

\subsection{Calculation of Anomalies}
 The mean anomaly of a binary system takes the form of a random value between $0$ and $2\pi$ in 
 order to simulate the observation at a random time. Calculating the 
 eccentric anomaly from the known mean anomaly requires the use of \eqref{mean_from_eccentric}, 
 which is solved analytically via the Newton-Raphson method (see appendix \ref{AnomConv}). The 
 true anomaly is then calculated using \eqref{true_from_eccentric}, and can 
 be used in \eqref{radial_velocity} to calculate what the observed radial 
 velocity of the O-star at that particular time would be. 

\subsection{Detection Condition}
The experiment requires two measurements of radial velocity taken 3 - 6 months 
apart, where the difference is randomly selected and uniformly distributed. The 
radial velocity difference $|v_i - v_j|$ between the two samples is used to 
determine the stars binarity under the following condition, formulated in 
\textcite{tarantulaviii}:

\begin{equation}
	\frac{|v_i - v_j|}{\sqrt{\sigma_i^2 + \sigma_j^2}} > 4
	\label{sigmarvcondition}
\end{equation}
and 
\begin{equation}
	|v_i - v_j| > 20 \unit{kms^{-1}}
	\label{cutoffrvcondition}
\end{equation}

Where $\sigma$ in \eqref{sigmarvcondition} is the $1\sigma$ error in the radial 
velocity measurement, taken to be $2.54 \unit{km^{-1}}$, as calculated as an 
average from observations made in \cite{tarantulaviii}, who used the VLT in a similar configuration. The errors reported were 
computed from Gaussian fitting of spectral lines. This condition ensures that 
the observation is of at least $4\sigma$ confidence.

The $20 \unit{kms^{-1}}$ limiting $\Delta v_r$ in \eqref{cutoffrvcondition} is 
chosen such that measurements below cannot be distinguish a binary from other 
radial velocity artifacts like stellar turbulence. 

\section{Analysis of Results}
The program was then used to create a distribution of 400 O-stars, of which 
51\% were part of a binary system. Then it simulated the observation of this 
population of stars, and by using the detection conditions discussed above, 
those which would be seen as binaries were found. It was found that the observed 
binary fraction for the sample of 400 O-stars was found to be 0.385, repeated runs of the simulation result in a standard deviation of $0.02$. A more 
accurate result was obtained by simulating 1 million O-stars, which gave an 
observed binary fraction of 0.374. This value is reduced from the input binary 
fraction by 27\%. 

\subsection{Statistical Tests}
\begin{tabular}{l|l} %KS TABLE
    \label{tab_ks}
    %caption!
    Parameter    & Probability	\\ \hline
    Eccentricity & 0.763  		\\
    Mass Ratio   & 0.040		\\
    log(P[days]) & 0.006		\\
    Inclination  & 0.134		\\
\end{tabular}


Table \ref{tab_ks} shows the results of the Kolmogorov Smirnov (KS) statistical 
test. The aim of this test is to quantify the correlation between the recovered 
distributions against the input distributions. The KS test was chosen as it 
gives more reliable results for distributions with low populations, such as the 
observed population which contains fewer than 200 data points. For this test, 
the input distribution was compared against the observed distribution was made 
up of only the detected binary systems from the population of 400 O-stars. 
The cumulative distributions on which the KS test was applied to are shown in 
Figure \ref{fig_CDFs}. 

\begin{figure*}[t!]
	\centering
	\begin{tabular}{ll}
		\includegraphics[scale=0.27]{../CDF_Plots/EccentricityCDF.eps} &
		\includegraphics[scale=0.27]{../CDF_Plots/MassRatioCDF.eps}	   \\
		\includegraphics[scale=0.27]{../CDF_Plots/Log10PeriodCDF.eps}  &
		\includegraphics[scale=0.27]{../CDF_Plots/InclinationCDF.eps}  \\
	\end{tabular}
	\caption{
		Cumulative Density Functions for eccentricity, mass ratio, 
		period and inclination.
	}
	\label{fig_CDFs}
\end{figure*}

\begin{Figure}
	\centering
	\includegraphics[width=\textwidth]{../root-plot/plots/e_1milPlots.pdf}
	\captionof{figure}{Recovered eccentricity distribution}
	\label{fig_e1mil}
\end{Figure}

The eccentricity distributions are shown in Figure \ref{fig_e1mil}, and are 
found to be in relatively good agreement. The results of the KS test imply that 
we can accept that the input distribution describes the recovered distribution 
with a confidence of 73.6\%.

The recovered distributions for mass ratio, inclination, and period have a much 
lower probability of being drawn from the input distribution, this indicates strong
observational biases toward sets of binary configurations using our observational method.

The mass ratio input distribution can be rejected with 96.0\% confidence, so it 
is evident that there is a need for a separate distribution to better quantify 
the observable distribution.

\begin{Figure}
	\centering
	\includegraphics[width=\textwidth]{../root-plot/plots/q_1milPlots.pdf}
	\captionof{figure}{Recovered mass ratio distribution}
	\label{fig_q1mil}
\end{Figure}

The data was also skewed in favour of systems with a high mass ratio, as shown 
in Figure \ref{fig_q1mil}. The cause of this was that if one star in the pair is 
significantly larger than the other (our simulation assumes a mass ratio of 
$\le 10$) the large star would experience a smaller radial velocity change than 
it would if the pair were of a similar mass. This is due to the fact that the 
both stars are orbiting their common centre of mass, which for small mass ratios 
will be very close to the largest star in the system. Thus, if only the larger 
star were observed, the measured $v_r$ dispersion would be low, and in some cases 
too low for the pair to be identified as binary stars. This makes it more 
difficult to observe systems with a low mass ratio, since the centre of mass of 
these systems will be much closer to the O-star.

\begin{Figure}
	\centering
	\includegraphics[width=\textwidth]{../root-plot/plots/i_1milPlots.pdf}
	\captionof{figure}{Recovered distribution of inclination}
	\label{fig_i1mil}
\end{Figure}

The input distribution for inclination can be rejected with a confidence of 
86.6\%. The loss of observable binary stars is present across all inclinations, 
as can be seen in Figure \ref{fig_i1mil}. This is due to other factors such as 
the system's mass ratio and period which are independent of its inclination. 
However it is noticeable that at low inclinations, a greater proportion of the 
intrinsic binaries are not observed. A binary system with a small inclination 
has its orbit inclined close to plane of the sky therefore it has a small 
component of velocity in the radial direction. For these alignments, the 
O-Star’s total velocity must be very large in order for a measurable component 
to be in the radial direction. Therefore it is only the fastest moving of the 
O-Stars that can be observed at low inclinations. At high inclinations, this 
restriction is lifted, meaning a greater proportion of O-Stars are detected to 
be in a binary system. 

The input distribution of $log$ period can be rejected with 99.4\% confidence. The 
observed number of binaries is reduced significantly for those with long 
periods, a trait that can be attributed to the observational bias inherent in 
Doppler shift measurements. From Kepler’s laws, the relationship of the measured 
radial velocity $v_r$ and the semi-major axis $a$ is:
\begin{equation}
 	v_r \propto  \frac{1}{a^{\frac{1}{2}}}
\end{equation}
and $a$ relates to the period $p$ such that
\begin{equation} 
	P^{2} \propto a^{3} 
\end{equation}
From these two relations, it follows that period relates to radial velocity by 
\begin{equation}
	P \propto v_r^{-3}
\end{equation}

As the radial velocity is what we are measuring, it follows that due to their 
smaller radial velocities, systems with larger periods will be harder to find 
using the Doppler method \parencite{phy229}.

\begin{Figure}
	\centering
	\includegraphics[width=\textwidth]{../root-plot/plots/logP_1milPlots.pdf}
	\captionof{figure}{Recovered periodicity distribution (from a sample of 1 million stars)}
	\label{fig_logP1mil}
\end{Figure}

The dip in Figure \ref{fig_logP1mil} between $\log(p)$ values of 1.9 and 2.3 approximately 
corresponds to $2.5 < p < 6.5$ months, which
encompasses the time at which the second observation is most likely to take place. 
For a star with a period of between 3 and 6 months it is likely that the both 
observations will occur when the star is at a similar position in its orbit, and 
hence will have a similar radial velocity. This lack of dispersion in radial 
velocity will cause the star to be incorrectly ignored when the program 
identifies binaries, causing the drop in observed binaries with a period within 
these time limits.

\section{Conclusions}
Our research shows that a general study of binary systems is feasible with the
FLAMES instrument, but care must be taken when attempting to derive the
the intrinsic distributions underlying the result. In particular, the returned
distribution is inaccurate at low mass ratios and at high periods, and the impact the sampling interval has on the resulting period distributions must be noted. Additionally, an under-reporting due to the inclination of the observed systems must be considered. Eccentricity shows relatively little variation from the intrinsic distribution, and excluding minor loss of low-eccentricity binaries, is largely an accurate representation. 
The observed binary fraction was found to be $0.37\pm0.02$, which if our assumptions and intrinsic parameter distributions are accurate, observations on the VLT should show a reduction of intrinsic binary fraction of ~27\%.

\printbibliography

\end{multicols}
\appendix
\lstset{language=C++, breaklines=true, prebreak=\linecontinue, numbers=left, mathescape=true}
\section{Source code}
The following appendices contain the source code to the simulation program used


\footnotesize
\subsection{Parameters.h}
\lstinputlisting{../Parameters.h}
\subsection{Random.h}
\lstinputlisting{../Random.h}
\subsection{AnomalyConversions.h}
\lstinputlisting{../AnomalyConversions.h}
\subsection{AnomalyConversions.cpp}
\label{AnomConv}
\lstinputlisting{../AnomalyConversions.cpp}
\subsection{RandomBinaryGenerator.h}
\lstinputlisting{../RandomBinaryGenerator.h}
\subsection{RandomBinaryGenerator.cpp}
\lstinputlisting{../RandomBinaryGenerator.cpp}
\subsection{Main.cpp}
\lstinputlisting{../Main.cpp}
\end{document}

#ifndef PARAMETERS_H 
#define PARAMETERS_H

#define O_STAR_AVERAGE_MASS         21.25   // $M_{\astrosun}$

// Exponents in power law distributions
#define MASSRATIO_KAPPA             -1.0
#define PERIOD_PI                   -0.45
#define ECCENTRICITY_ETA            -0.51

#define BINARY_FRACTION             0.51    // Fraction of all stars which are binaries
#define RV_DETECTION_THRESHOLD      35000.0 //  $ms^-1$

#endif


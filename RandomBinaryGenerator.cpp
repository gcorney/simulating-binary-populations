#include "RandomBinaryGenerator.h"
#include "Random.h"
#include "AnomalyConversions.h"
#include "Parameters.h"
#include <iostream>
#include <cmath>
#include <cassert>

using std::cout;
using std::endl;

//! Generate a random binary
Binary::Binary() 
    : m_massRatio(powerLawRand(MASSRATIO_KAPPA, .1, 1))
    , m_period(pow(10, powerLawRand(PERIOD_PI, .15, 3.5)))
    , m_eccentricity(powerLawRand(ECCENTRICITY_ETA, .00001, .9))

    , m_meanAnomaly(uniformRand(0, 2 * M_PI))
    , m_eccentricAnomaly(meanToEccentric(m_meanAnomaly, m_eccentricity))
    , m_trueAnomaly(eccentricToTrue(m_eccentricAnomaly, m_eccentricity))

    // Isotropic
    , m_longitudePeriastron(uniformRand(0, M_PI))

    // Derrived from generating a random oribal plane and finding the angle against the z direction
    // uniform spherical distribution detailed here (non trivial): http://mathworld.wolfram.com/SpherePointPicking.html
    , m_inclination(std::acos(uniformRand(0, 1))) //0-90 degs, Littlefair's suggestion

    , m_a(calculate_a())
    , m_aStar(calculate_aStar())
{}

Binary::Binary(
    double massRatio, 
    double period, 
    double eccentricity, 
    double meanAnomaly,
    double eccentricAnomaly, 
    double trueAnomaly, 
    double longitudePeriastron, 
    double inclination, 
    double aStar, 
    double a)
    : m_massRatio(massRatio)
    , m_period(period)
    , m_eccentricity(eccentricity)
    , m_meanAnomaly(meanAnomaly)
    , m_eccentricAnomaly(eccentricAnomaly)
    , m_trueAnomaly(trueAnomaly)
    , m_longitudePeriastron(longitudePeriastron)
    , m_inclination(inclination)
    , m_a(a)
    , m_aStar(aStar)
{}

Binary::~Binary()
{}

double Binary::calculate_a(){
    const double G = 6.67E-11; //Gravitational constant, SI units
    const double MSun = 2E30; //kg
    double period_seconds = m_period*24*60*60;
    double secondaryStarMass = m_massRatio * O_STAR_AVERAGE_MASS;  //yey
    double a = pow(((G*(O_STAR_AVERAGE_MASS+secondaryStarMass)*MSun*period_seconds*period_seconds)/4*M_PI),(double)1/3);
    return a;
}

double Binary::calculate_aStar() {
    double aStar = m_a/(1+(1.0/m_massRatio));
    return aStar;
}

void Binary::progressOrbit(double days){
    //How many periods have passed?
    double remainingOrbitFraction = fmod(days, m_period)/m_period;

    m_meanAnomaly = fmod(m_meanAnomaly+remainingOrbitFraction*(2*M_PI), 2*M_PI); 
    m_eccentricAnomaly = meanToEccentric(m_meanAnomaly, m_eccentricity);
    m_trueAnomaly = eccentricToTrue(m_eccentricAnomaly, m_eccentricity);
}

double Binary::observedRadialVelocity() const // meters / second
{   
    double period_seconds = m_period*24*60*60;
    double K = 2 * M_PI * m_aStar * std::sin(m_inclination) / (period_seconds * std::sqrt(1 - std::pow(m_eccentricity, 2)));
    return K * (std::cos(m_trueAnomaly + m_longitudePeriastron) + m_eccentricity * cos(m_longitudePeriastron));
}


void Binary::setVelocityDifference(double dV)
{
	m_DV = dV;
}


/*static*/ void Binary::printTSVHeader(FILE* filename)
{
    fprintf(filename,"%-13s %-13s %-13s %-13s %-13s %-13s %-13s %-13s %-13s %-13s %-13s %-13s\n",
        "eccentricity", "massRatio", "period", "longPeri",
        "inclination", "a*", "a", "trueAnomaly", "eccAnomaly",
        "meanAnomaly", "obsRadVel", "dRadVel");
}


void Binary::printTSV(FILE* filename) const
{
    fprintf(filename, "%-13e %-13e %-13e %-13e %-13e %-13e %-13e %-13e %-13e %-13e %012.5e %012.5e\n",
        m_eccentricity, m_massRatio, m_period, m_longitudePeriastron,
        m_inclination, m_aStar, m_a, m_trueAnomaly, m_eccentricAnomaly,
        m_meanAnomaly, observedRadialVelocity(), m_DV);
}


/* 
-- Todo --
 - Account for cases where NR fails (gradient of zero, etc) see 'numerical recipies'
*/
#include "AnomalyConversions.h"
#include "Random.h"
#include <cmath>
#include <stdexcept>

double meanToTrue(double M, double e){
    return eccentricToTrue(meanToEccentric(M,e), e);
}

double meanToEccentric(double M, double e){
    int maxIterations = 3000;
	double threshold = pow(10,-15);
    
	if(e >= 1)
		throw std::domain_error("meanToEccentric() only supports elliptical orbits");

	double E = M;// First Guess
	double E_last;

	// If the inital guess is bad, we can try again, this loops allows the starting point to be changed
	int attemptLimit = 20;
	while(attemptLimit>0){
		// Newton-Rhapson Method for Kepler's equation
		// $M = E - e sin(E)$
		// $M' = 1 - e cos(E)$
		for (int i = 0; i < maxIterations; ++i)
		{
			E_last = E;
			E = E + (M + e * std::sin(E) - E)/(1 - e * std::cos(E));
			
			if( std::abs(E - E_last) <= threshold ) return E;
		}

		E = M + uniformRand(-1,1)*6;
		attemptLimit --;
	}		

	throw std::logic_error("Newton-Rhapson Approximation has failed (meanToEccentric)");
}

double eccentricToTrue(double E, double e){
    return 2*atan2(sqrt(e+1)*sin(E/2), sqrt(1-e)*cos(E/2));
}
#ifndef RANDOM_H
#define RANDOM_H
#include <ctime>
#include <random>

static std::mt19937_64 twister(std::time(nullptr));


static inline double uniformRand(double min = 0., double max = 1.)
{
    std::uniform_real_distribution<> dist(min, max);
    return dist(twister);
}

static inline double powerLawRand(double exponent, double lowerBound, double upperBound) 
{
    //See http://mathworld.wolfram.com/RandomNumber.html for derrivation

    //Power law PDFs with a negative exponent tend to infinity at 0; you must use a lowerBound > 0 
    if (exponent <= 1 && lowerBound <= 0)
        return 0;
    double r = uniformRand(); //uniform rand [0,1]
    double n = exponent;
    if (n !=-1) 
        return pow((pow(upperBound, n+1) - pow(lowerBound, n+1))*r + pow(lowerBound, n+1), (1/(n+1)));
    else //Special case where exponent = -1
        return lowerBound * pow(M_E, r/(1 / log(upperBound/lowerBound))); 
}

#endif
